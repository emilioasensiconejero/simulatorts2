from opcua import Client
from opcua import ua


class OPCClient(Client):
    def __init__(self, url, timeout=4):
        super().__init__(url, timeout=4)
        self.expand_list = []
        self.selected = self.get_root_node()

    def setValue(self, node, value):
        variant_type = node.get_data_type_as_variant_type()
        variant = ua.uatypes.Variant(value, variant_type)
        data_value = ua.DataValue()
        data_value.Value = variant
        node.set_data_value(data_value, variant_type)

    def getValue(self, node):
        return node.get_value()

    def getName(self, node):
        return str(node.get_browse_name()).split("(")[1].split(")")[0]

    def applyVal(self, node, val, feedback):
        try:
            self.setValue(node, type(node.get_value())(val))
            feedback.setText(str(node.get_value()))
        except Exception as e:
            print("Couldn't set value")
            print(e)
